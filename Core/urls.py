from django.contrib import admin
from django.urls import include, path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

scemaview = get_schema_view(
    openapi.Info(
        title="CBT API",
        default_version="v1",
        description="Siakad API documentation",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="trioagung@uim.ac.id"),
        license=openapi.License(name="BSD License"),
    ),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', scemaview.with_ui('swagger')),
    path('api/', include("users.urls")),
    path('api/', include("cbt.urls")),
]



