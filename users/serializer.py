from rest_framework import serializers

from . import services
from . import models
# from pmb import serializer as profile_serialiser

class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    email = serializers.CharField()
    password = serializers.CharField(write_only=True)

    def to_internal_value(self, data):
        data = super().to_internal_value(data)

        return services.UserDataClass(**data)

class UserLogin(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ['email', 'password']

class UsernameSerializer(serializers.ModelSerializer):
    # profile = profile_serialiser.ProfileSerializer(read_only=True)
    class Meta:
        model = models.User
        fields = ['first_name', 'last_name', 'email']

class UserCreate(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ['first_name', 'last_name', 'email', 'password']
