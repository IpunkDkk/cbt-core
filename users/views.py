from rest_framework import views, response, exceptions, permissions,  generics
from django.contrib.auth import login, authenticate

from . import serializer as user_serializer
from . import services, authentication
from . import models

class RegisterApi(generics.GenericAPIView):
    queryset = models.User.objects.all()
    serializer_class = user_serializer.UserCreate
    def post(self, request):
        serializer = user_serializer.UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        serializer.instance = services.create_user(user_dc=data)

        return response.Response(data=serializer.data)


class LoginApi(generics.GenericAPIView):
    queryset = models.User.objects.all()
    serializer_class = user_serializer.UserLogin
    def post(self, request):
        if not request.data:
            raise exceptions.ValidationError("No data provided")
        email = request.data["email"]
        password = request.data["password"]

        user = services.user_email_selector(email=email)

        if user is None:
            raise exceptions.AuthenticationFailed("Invalid Credentials")

        if not user.check_password(raw_password=password):
            raise exceptions.AuthenticationFailed("Invalid Credentials")

        login(request, authenticate(username=email, password=password))
        token = services.create_token(user_id=user.id)
        resp = response.Response()

        resp.set_cookie(key="jwt", value=token, httponly=True)
        resp.data = {
            'jwt' : token
        }

        return resp


class UserApi(views.APIView):
    """
    This endpoint can only be used
    if the user is authenticated
    """

    authentication_classes = (authentication.CustomUserAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        user = request.user

        serializer = user_serializer.UserSerializer(user)

        return response.Response(serializer.data)


class LogoutApi(views.APIView):
    authentication_classes = (authentication.CustomUserAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        resp = response.Response()
        resp.delete_cookie("jwt")
        resp.delete_cookie("sessionid")
        resp.data = {"message": "so long farewell"}

        return resp