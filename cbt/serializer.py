from rest_framework import serializers
from . import models
from users.serializer import UsernameSerializer as UserAcc


class AnsweredSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Answered
        fields = "__all__"


class KodeSoalSerializer(serializers.ModelSerializer):
    user = UserAcc(read_only=True)
    class Meta:
        model = models.KodeSoal
        fields = "__all__"


class SoalSerializer(serializers.ModelSerializer):
    # kode = KodeSoalSerializer(read_only=True)
    # # answer = AnswerSerializer(read_only=True)
    # answered = AnsweredSerializer(read_only=True)
    class Meta:
        model = models.Soal
        fields = "__all__"

class ViewSoalSerializer(serializers.ModelSerializer):
    kode = KodeSoalSerializer(read_only=True)
    # answer = AnswerSerializer(read_only=True)
    answered = AnsweredSerializer(read_only=True)
    class Meta:
        model = models.Soal
        fields = "__all__"

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Answer
        fields = "__all__"


class SoalAnsweSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SoalAnswer
        fields = "__all__"

class ViewSoalAnsweSerializer(serializers.ModelSerializer):
    # soal = SoalSerializer(read_only=True)
    answer = AnswerSerializer(read_only=True)

    class Meta:
        model = models.SoalAnswer
        fields = "__all__"