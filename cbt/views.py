from rest_framework import response, exceptions, permissions,  generics


from . import serializer as cbt
from . import models
from users import authentication

class KodeSoalApi(generics.GenericAPIView):
    authentication_classes = (authentication.CustomUserAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = cbt.KodeSoalSerializer
    queryset = models.KodeSoal.objects.all()

    def post(self, request):
        if not request.data:
            raise exceptions.ValidationError("No Data Provided")
        serializer  = cbt.KodeSoalSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Response(serializer.data)
    def get(self, request):
        kode_id = request.GET['kode']
        kodeSoal = models.Soal.objects.filter(kode=kode_id)
        serializer = cbt.SoalSerializer(kodeSoal, many=True)
        return response.Response(serializer.data)


class SoalApi(generics.GenericAPIView):
    serializer_class = cbt.SoalSerializer
    queryset = models.Soal.objects.all()
    authentication_classes = (authentication.CustomUserAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        soal = models.Soal.objects.all()
        serializer = cbt.SoalSerializer(soal, many=True)
        return response.Response(serializer.data)

    def post(self, request):
        if not request.data:
            raise exceptions.ValidationError("No Data Provided")
        serializer = cbt.SoalSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Response(serializer.data)

class AnswerApi(generics.GenericAPIView):
    serializer_class = cbt.AnswerSerializer
    queryset = models.Answer.objects.all()
    authentication_classes = (authentication.CustomUserAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        if not request.data:
            raise exceptions.ValidationError("No Data Provided")
        serializer = cbt.AnswerSerializer(data=request.data)
        serializer.is_valid()
        serializer.save()
        return response.Response(serializer.data)
    def get(self, request):
        id_soal = request.GET['id_soal']
        answer = models.Answer.objects.filter(soal=id_soal)
        serializer = cbt.AnswerSerializer(answer, many=True)
        return response.Response(serializer.data)

class SoalAnswerApi(generics.GenericAPIView):
    serializer_class = cbt.SoalAnsweSerializer
    queryset = models.SoalAnswer.objects.all()
    authentication_classes = (authentication.CustomUserAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        if not request.data:
            raise exceptions.ValidationError("No Data Provided")
        serializer = cbt.SoalAnsweSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response.Response(serializer.data)
    def get(self, request):
        id_soal = request.GET['id_soal']
        soalanswer = models.SoalAnswer.objects.filter(soal=id_soal)
        serializer = cbt.ViewSoalAnsweSerializer(soalanswer, many=True)
        return response.Response(serializer.data)