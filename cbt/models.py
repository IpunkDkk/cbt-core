from django.db import models
from users import models as model_user


class KodeSoal(models.Model):
    kode = models.CharField(verbose_name="Kode Soal", max_length=16)
    user = models.ForeignKey(model_user.User, on_delete=models.CASCADE)


class Soal(models.Model):
    kode = models.ForeignKey(KodeSoal, on_delete=models.CASCADE)
    soal = models.CharField(verbose_name="Soal", max_length=255)


class Answer(models.Model):
    # soal =
    soal = models.ForeignKey(Soal, on_delete=models.CASCADE)
    answer = models.CharField(verbose_name="Jawaban", max_length=255)
    isCorrect = models.BooleanField(default=False, verbose_name="Apakah Ini Jawaban Benar")


class SoalAnswer(models.Model):
    soal = models.ForeignKey(Soal, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)


class Answered(models.Model):
    soal = models.ForeignKey(Soal, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    user = models.ForeignKey(model_user.User, on_delete=models.CASCADE)
