from django.urls import path

from . import views

urlpatterns = [
    path("soal", views.SoalApi.as_view(), name="soal"),
    path("kode", views.KodeSoalApi.as_view(), name="kodesoal"),
    path("answer", views.AnswerApi.as_view(), name='answer'),
    path("soalanswer", views.SoalAnswerApi.as_view(), name="soalanswer"),

]
